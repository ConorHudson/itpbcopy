import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import java.awt.Color;

public class Primary extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	int xx, xy;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Primary frame = new Primary();
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Primary() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 869, 602);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				xx = e.getX();
				xy = e.getY();
			}
		});
		contentPane.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				int x = e.getXOnScreen();
				int y = e.getYOnScreen();
				Primary.this.setLocation(x - xx, y - xy);
			}
		});
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel Config = new JLabel("");
		Config.setIcon(new ImageIcon(Primary.class.getResource("/Images/settings-cog.png")));
		Config.setBounds(785, 522, 60, 60);
		contentPane.add(Config);
		
		JLabel Terminal = new JLabel("");
		Terminal.setIcon(new ImageIcon(Primary.class.getResource("/Images/terminal-512.png")));
		Terminal.setBounds(10, 522, 60, 60);
		contentPane.add(Terminal);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Primary.class.getResource("/Images/ITP_logo.png")));
		lblNewLabel.setBounds(10, 10, 164, 60);
		contentPane.add(lblNewLabel);
		
	}
}
